package com.rss.zuevayulya.view;

import com.rss.zuevayulya.presenter.vo.News;

import java.util.List;

/**
 * Created by Yulya on 25.09.2016.
 */
public interface NewsListView extends View {
    void showData(List<News> vo);
    void showEmptyList();
}
