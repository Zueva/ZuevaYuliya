package com.rss.zuevayulya.view;

/**
 * Created by Yulya on 24.09.2016.
 */
public interface View {

    void showError(String error);
    void showLoading();
    void hideLoading();
}
