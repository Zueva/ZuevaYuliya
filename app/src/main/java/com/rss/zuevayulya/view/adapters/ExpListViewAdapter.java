package com.rss.zuevayulya.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rss.zuevayulya.R;
import com.rss.zuevayulya.di.DaggerViewComponent;
import com.rss.zuevayulya.di.ViewComponent;
import com.rss.zuevayulya.di.ViewDynamicModule;
import com.rss.zuevayulya.presenter.vo.News;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Yulya on 17.09.2016.
 */
public class ExpListViewAdapter extends BaseExpandableListAdapter {

    @Inject
    protected Picasso picasso;

    private int defaultImageId = R.drawable.ic_image_black_24dp;
    private List<News> newsList = new ArrayList();
    private Context mContext;
    private ViewComponent viewComponent;

    public void setNewsList(List<News> news){
        this.newsList = news;
        notifyDataSetChanged();
    }

    public ExpListViewAdapter(Context context) {
        this.mContext = context;
        viewComponent = DaggerViewComponent.builder()
                .viewDynamicModule(new ViewDynamicModule(this.mContext))
                .build();
        viewComponent.inject(this);
    }
    @Override
    public int getGroupCount() {
        return newsList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return newsList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return newsList.get(groupPosition).getDescription();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder viewHolder;
        News news = (News) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group_view, null);
            viewHolder = new GroupViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GroupViewHolder)convertView.getTag();
        }

        viewHolder.title.setText(news.getTitle());
        picasso.load(news.getEnclosure()).placeholder(getDefaultImageId()).into(viewHolder.image);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.child_view, null);
            viewHolder = new ChildViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ChildViewHolder) convertView.getTag();
        }

        viewHolder.description.setText(getChild(groupPosition, childPosition).toString());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class GroupViewHolder {

        View groupView;

        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.img)
        ImageView image;

        public GroupViewHolder(View view) {
            groupView = view;

            ButterKnife.bind(this, view);
        }
    }

    class ChildViewHolder {
        View childView;

        @Bind(R.id.description)
        TextView description;

        public ChildViewHolder(View view) {
            childView = view;

            ButterKnife.bind(this, view);
        }
    }

    public int getDefaultImageId()
    {
        return defaultImageId;
    }
}
