package com.rss.zuevayulya.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rss.zuevayulya.R;
import com.rss.zuevayulya.di.DaggerViewComponent;
import com.rss.zuevayulya.di.ViewComponent;
import com.rss.zuevayulya.di.ViewDynamicModule;
import com.rss.zuevayulya.presenter.NewsListPresenter;
import com.rss.zuevayulya.presenter.vo.News;
import com.rss.zuevayulya.view.adapters.ExpListViewAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RssListActivity extends AppCompatActivity  implements NewsListView{

    @Bind(R.id.toolbar_progress_bar)
    protected ProgressBar progressBar;

    @Bind(R.id.expListView)
    protected ExpandableListView expListView;

    private ExpListViewAdapter adapter;
    private ViewComponent viewComponent;

    @Inject
    protected NewsListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_list);
        ButterKnife.bind(this);

        viewComponent = DaggerViewComponent.builder()
                .viewDynamicModule(new ViewDynamicModule((NewsListView) this))
                .build();
        viewComponent.inject(this);

        presenter.OnBindView();

        adapter = new ExpListViewAdapter(this);
        expListView.setAdapter(adapter);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.OnStop();
        }
    }

    @Override
    public void showData(List<News> news) {
        adapter.setNewsList(news);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyList() {
        Toast.makeText(this, getString(R.string.empty_data), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }
}
