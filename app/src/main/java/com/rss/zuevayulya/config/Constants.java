package com.rss.zuevayulya.config;

/**
 * Created by Yulya on 27.09.2016.
 */

public interface Constants {
    String UI_THREAD = "UI_THREAD";
    String IO_THREAD = "IO_THREAD";

    String BASE_URL = "https://lenta.ru/rss/";
}
