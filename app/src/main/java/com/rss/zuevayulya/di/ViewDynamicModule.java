package com.rss.zuevayulya.di;

import android.content.Context;

import com.rss.zuevayulya.presenter.NewsListPresenter;
import com.rss.zuevayulya.view.NewsListView;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yulya on 27.09.2016.
 */

@Module
public class ViewDynamicModule {
    private NewsListView view;
    private Context context;

    public ViewDynamicModule(NewsListView view){
        this.view = view;
    }

    public ViewDynamicModule(Context context){
        this.context = context;
    }

    @Provides
    NewsListPresenter provideNewsListPresenter() {
        return new NewsListPresenter(view);
    }

    @Provides
    Picasso providePicasso() {
        return Picasso.with(this.context);
    }
}
