package com.rss.zuevayulya.di;

import com.rss.zuevayulya.model.Model;
import com.rss.zuevayulya.model.ModelImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by Yulya on 27.09.2016.
 */

@Module
public class PresenterModule {

    @Provides
    @Singleton
    Model provideNewsData() {
        return new ModelImpl();
    }

    @Provides
    Subscription provideSubscription() {
        return Subscriptions.empty();
    }
}
