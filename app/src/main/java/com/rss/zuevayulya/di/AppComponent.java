package com.rss.zuevayulya.di;

import com.rss.zuevayulya.model.ModelImpl;
import com.rss.zuevayulya.presenter.NewsListPresenter;

import javax.inject.Singleton;

import dagger.Component;
/**
 * Created by Yulya on 27.09.2016.
 */

@Singleton
@Component(modules = {ModelModule.class, PresenterModule.class})
public interface AppComponent {

    void inject(ModelImpl dataNews);

    void inject(NewsListPresenter newsListPresenter);
}
