package com.rss.zuevayulya.di;

import com.rss.zuevayulya.view.RssListActivity;
import com.rss.zuevayulya.view.adapters.ExpListViewAdapter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Yulya on 27.09.2016.
 */

@Singleton
@Component(modules = {ViewDynamicModule.class})
public interface ViewComponent {

    void inject(RssListActivity rssListActivity);

    void inject(ExpListViewAdapter listViewAdapter);
}
