package com.rss.zuevayulya;

import android.app.Application;
import com.rss.zuevayulya.di.AppComponent;
import com.rss.zuevayulya.di.DaggerAppComponent;

/**
 * Created by Yulya on 18.09.2016.
 */
public class RssApplication extends Application {

    private static AppComponent component;
    public static AppComponent getComponent() {
        return component;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent();
    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .build();
    }
}
