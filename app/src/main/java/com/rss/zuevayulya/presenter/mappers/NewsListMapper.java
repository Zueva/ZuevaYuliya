package com.rss.zuevayulya.presenter.mappers;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.rss.zuevayulya.model.dto.RSS;
import com.rss.zuevayulya.presenter.vo.News;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

import rx.functions.Func1;

/**
 * Created by Yulya on 25.09.2016.
 */
public class NewsListMapper implements Func1<RSS, List<News>> {

    @Inject
    public NewsListMapper() {

    }

    @Override
    public List<News> call(final RSS newsDTOs) {
        if (newsDTOs == null){
            return null;
        }

        List<News> newsList = Observable.from(newsDTOs.getChannel().getItems())
                .map(item -> new News(item.getTitle(), item.getEnclosure().getUrl(), item.getDescription(), item.getPubDate()))
                .toList()
                .toBlocking()
                .first();
        Collections.sort(newsList, (p1, p2) -> p2.getDate().compareTo(p1.getDate()));
        return newsList;
    }
}
