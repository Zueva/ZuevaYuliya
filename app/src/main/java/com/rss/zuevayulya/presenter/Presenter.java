package com.rss.zuevayulya.presenter;

/**
 * Created by Yulya on 24.09.2016.
 */
public interface Presenter {
    void OnStop();
    void OnBindView();
}
