package com.rss.zuevayulya.presenter;

import android.os.Bundle;

import com.rss.zuevayulya.RssApplication;
import com.rss.zuevayulya.presenter.vo.News;
import com.rss.zuevayulya.model.Model;
import com.rss.zuevayulya.presenter.mappers.NewsListMapper;
import com.rss.zuevayulya.view.NewsListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;

/**
 * Created by Yulya on 24.09.2016.
 */
public class NewsListPresenter implements Presenter {

    private static final String BUNDLE_NEWS_DATA = "newsData";

    @Inject
    protected Model model;

    @Inject
    protected NewsListMapper newsListMapper;

    @Inject
    protected Subscription subscription;

    private NewsListView view;
    private List<News> newsList;

    @Inject
    public NewsListPresenter() {

    }

    public NewsListPresenter(NewsListView view) {
        RssApplication.getComponent().inject(this);
        this.view = view;
    }

    @Override
    public void OnBindView() {

        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        view.showLoading();
            subscription = model.getNewsList()
                .map(newsListMapper)
                .subscribe(new Observer<List<News>>() {
            @Override
            public void onCompleted() {
                view.hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                view.hideLoading();
                view.showError(e.toString());
            }

            @Override
            public void onNext(List<News> news) {
                if (news != null && !news.isEmpty()) {
                    newsList = news;
                    view.showData(news);
                }else {
                    view.showEmptyList();
                }
            }
        });
    }

    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            newsList = (List<News>) savedInstanceState.getSerializable(BUNDLE_NEWS_DATA);
        }
        if (newsIsNotEmpty()) {
            view.showData(newsList);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (newsIsNotEmpty()) {
            outState.putSerializable(BUNDLE_NEWS_DATA, new ArrayList<>(newsList));
        }
    }

    @Override
    public void OnStop() {
        if(!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    private boolean newsIsNotEmpty() {
        return (newsList != null && !newsList.isEmpty());
    }
}
