package com.rss.zuevayulya.presenter.vo;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Yulya on 24.09.2016.
 */
public class News implements Serializable{
    static SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
    private String title;
    private String enclosure;
    private String description;
    private Date date;

    public News(String title, String enclosure, String description, String date){
        this.title = title;
        this.enclosure = enclosure;
        this.description = description;
        setDate(date);
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getEnclosure ()
    {
        return enclosure;
    }

    public void setEnclosure (String enclosure)
    {
        this.enclosure = enclosure;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public void setDate (String date)
    {
        try {
            this.date = formatter.parse(date.trim());
        }  catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getDate() {
        return date;
    }

}
