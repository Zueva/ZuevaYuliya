package com.rss.zuevayulya.model.api;

import com.rss.zuevayulya.config.Constants;
import com.rss.zuevayulya.model.dto.RSS;
import retrofit2.http.GET;
import rx.Observable;
/**
 * Created by Yulya on 24.09.2016.
 */
public interface ApiInterface {
    @GET(Constants.BASE_URL)
    Observable<RSS> getNews();
}
