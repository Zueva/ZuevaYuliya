package com.rss.zuevayulya.model.api;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by Yulya on 24.09.2016.
 */
public class ApiModule {

    public static ApiInterface getApiInterface(String url){

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        chain -> {
                            Request original = chain.request();
                            Request.Builder requestBuilder = original.newBuilder()
                                    .method(original.method(), original.body());

                            Request request = requestBuilder.build();
                            return chain.proceed(request);
                        })
                .build();

        Retrofit.Builder builder = new Retrofit.Builder().
                baseUrl(url)
                .client(httpClient)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        ApiInterface apiInterface = builder.build().create(ApiInterface.class);
        return  apiInterface;
    }
}
