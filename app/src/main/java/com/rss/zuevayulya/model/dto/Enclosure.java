package com.rss.zuevayulya.model.dto;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Yulya on 29.09.2016.
 */

@Root(name="enclosure", strict=false)
public class Enclosure {
    @Attribute(name = "url",  empty = "")
    private String url;

    public String getUrl ()
    {
        return url;
    }
}
