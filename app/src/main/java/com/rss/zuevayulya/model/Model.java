package com.rss.zuevayulya.model;

import com.rss.zuevayulya.model.dto.RSS;
import rx.Observable;

/**
 * Created by Yulya on 24.09.2016.
 */
public interface Model {
    Observable<RSS> getNewsList();
}
