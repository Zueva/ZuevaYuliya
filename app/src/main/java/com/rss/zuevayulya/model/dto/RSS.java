package com.rss.zuevayulya.model.dto;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Yulya on 28.09.2016.
 */

@Root(name = "rss", strict=false)
public class RSS {
    @Attribute(name = "version")
    String version;

    @Element
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }

    }

