package com.rss.zuevayulya.model;

import com.rss.zuevayulya.RssApplication;
import com.rss.zuevayulya.config.Constants;
import com.rss.zuevayulya.model.api.ApiInterface;
import com.rss.zuevayulya.model.dto.RSS;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;

/**
 * Created by Yulya on 24.09.2016.
 */
public class ModelImpl implements Model{

    private final Observable.Transformer shedulersTransformer;

    @Inject
    protected ApiInterface apiInterface;

    @Inject
    @Named(Constants.UI_THREAD)
    Scheduler uiThread;

    @Inject
    @Named(Constants.IO_THREAD)
    Scheduler ioThread;

    public ModelImpl() {
        RssApplication.getComponent().inject(this);
        shedulersTransformer = o -> ((Observable) o).subscribeOn(ioThread)
                .observeOn(uiThread);
    }

    @Override
    public Observable<RSS> getNewsList() {
        return apiInterface.getNews()
                .compose(shedulersTransformer);
    }

}
