package com.rss.zuevayulya.model.dto;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Yulya on 28.09.2016.
 */

@Root(name="Channel", strict=false)
public class Channel {

    @ElementList(inline=true, required=false)
    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }
}



