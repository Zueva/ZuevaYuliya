package com.rss.zuevayulya.model.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="item", strict=false)
public class Item {

    @Element(name = "title")
    private String title;
    @Element(name = "description")
    private String description;

    @Element(name = "enclosure", required=false)
    private Enclosure enclosure;

    @Element(name = "pubDate")
    private String pubDate;


    public String getTitle ()
    {
        return title;
    }

    public String getPubDate ()
    {
        return pubDate;
    }

    public String getDescription ()
    {
        return description;
    }

    public Enclosure getEnclosure() {
        if (enclosure == null) {
            return new Enclosure();
        }
        return enclosure;
    }

}